﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Awesomium.Core;

namespace FinartSpikeWithFormsWithAwesomium
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            webControl.Source = textBoxForURL.Text.ToUri();
        }

        private void Awesomium_Windows_Forms_WebControl_DocumentReady(object sender, DocumentReadyEventArgs e)
        {
            JSValue jsExecutionResult = webControl.ExecuteJavascriptWithResult("document.getElementById('bridgeElement')");
            Console.WriteLine(jsExecutionResult.ToString());
            if (jsExecutionResult.IsObject)
            {
                Console.WriteLine("Element found ");
                JSObject jsObject = jsExecutionResult;
                jsObject.Bind("onclick", new JSFunctionHandler(elementOnclick));
            }
        }

        private JSValue elementOnclick(JSValue[] arguments)
        {
            Console.WriteLine("Web event caught");
            return JSValue.Undefined;
        }
    }
}
