﻿namespace FinartSpikeWithFormsWithAwesomium
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxForURL = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.webControl = new Awesomium.Windows.Forms.WebControl(this.components);
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "URL :";
            // 
            // textBoxForURL
            // 
            this.textBoxForURL.Location = new System.Drawing.Point(53, 6);
            this.textBoxForURL.Name = "textBoxForURL";
            this.textBoxForURL.Size = new System.Drawing.Size(795, 20);
            this.textBoxForURL.TabIndex = 1;
            this.textBoxForURL.Text = "http://192.168.1.104:3000";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(854, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(46, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "GO";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // webControl
            // 
            this.webControl.Location = new System.Drawing.Point(15, 35);
            this.webControl.Size = new System.Drawing.Size(885, 465);
            this.webControl.TabIndex = 3;
            this.webControl.DocumentReady += new Awesomium.Core.DocumentReadyEventHandler(this.Awesomium_Windows_Forms_WebControl_DocumentReady);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(905, 505);
            this.Controls.Add(this.webControl);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBoxForURL);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxForURL;
        private System.Windows.Forms.Button button1;
        private Awesomium.Windows.Forms.WebControl webControl;
    }
}

