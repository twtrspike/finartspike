﻿using Microsoft.Win32;
using mshtml;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FinartSpike
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, MessageListener
    {
        LocalServer localServer;

        public MainWindow()
        {
            InitializeComponent();
            // WPF browser actiginda compatibility mode ile birlikte aciyor (IE7) 
            // Bu durumda redux calismiyor. Bunu ebgellemek icin asagidaki method cagriliyor
            // Registry de bu uygulama icin compatibilty mode ayarlaniyor.
            // @alperk
            SetBrowserEmulationMode();
            localServer = new LocalServer();
            localServer.StartServer();
            MessageRelay.INSTANCE.register(this);
        }

        private void openURLButton_Click(object sender, RoutedEventArgs e)
        {
            string URL = urlTextBox.Text;
            try
            {
                logBox.Text = "";
                webBrowser.Navigate(URL);
                
            }
            catch (Exception err)
            {
                logBox.Text += "Exception "+err.Message;
            }
        }

        private void webBrowser_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void webBrowser_LoadCompleted(object sender, NavigationEventArgs e)
        {
            HTMLDocument doc = (HTMLDocument)webBrowser.Document;
            
            IHTMLElement bridgeElement = doc.getElementById("bridgeElement");
            if(bridgeElement != null)
            {
                ((HTMLInputTextElementEvents2_Event)bridgeElement).onclick += new HTMLInputTextElementEvents2_onclickEventHandler(ClickEventHandler);
            }
        }

        private bool ClickEventHandler(IHTMLEventObj occurredEvent)
        {
            var message = occurredEvent.type + "-" + occurredEvent.srcElement.tagName + "-" + occurredEvent.returnValue + "\n";
            Console.WriteLine(message);
            logBox.Text += message;
            return true;
        }


        
        private void SetBrowserFeatureControlKey(string feature, string appName, uint value)
        {
            using (var key = Registry.CurrentUser.CreateSubKey(
                String.Concat(@"Software\Microsoft\Internet Explorer\Main\FeatureControl\", feature),
                RegistryKeyPermissionCheck.ReadWriteSubTree))
            {
                key.SetValue(appName, (UInt32)value, RegistryValueKind.DWord);
            }
        }

        public void SetBrowserEmulationMode()
        {
            var fileName = System.IO.Path.GetFileName(Process.GetCurrentProcess().MainModule.FileName);

            if (String.Compare(fileName, "devenv.exe", true) == 0 || String.Compare(fileName, "XDesProc.exe", true) == 0)
                return;
            UInt32 mode = 10000;
            SetBrowserFeatureControlKey("FEATURE_BROWSER_EMULATION", fileName, mode);

        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            localServer.StopServer();
        }

        public void MessageArrived(object message)
        {
            // logBox.Text += message + "\n";
        }
    }


    public class MessageRelay
    {
        public static readonly MessageRelay INSTANCE = new MessageRelay();
        
        IList<MessageListener> listeners;

        private MessageRelay()
        {
            listeners = new List<MessageListener>();
        }

        public void RelayMessage(object message)
        {
            Console.WriteLine("Listener count : " + listeners.Count);
            foreach (MessageListener listener in listeners)
            {
                listener.MessageArrived(message);
            }
        }

        public void register(MessageListener listener)
        {
            listeners.Add(listener);
        }
    }

    public interface MessageListener
    {
        void MessageArrived(object message);
    }
}
