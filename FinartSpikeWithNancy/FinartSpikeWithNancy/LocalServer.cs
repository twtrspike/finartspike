﻿using Nancy;
using Nancy.Hosting.Self;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinartSpikeWithNancy
{
    public class LocalServer
    {
        private NancyHost host;

        public LocalServer()
        {
            var hostConfiguration = new HostConfiguration();
            var urlReservations = new UrlReservations();
            urlReservations.CreateAutomatically = true;

            hostConfiguration.UrlReservations = urlReservations;
            host = new NancyHost(hostConfiguration, new Uri("http://localhost:1234"));

        }

        public void StartServer()
        {
            host.Start();
        }

        public void StopServer()
        {
            host.Dispose();
        }


    }

    public class HelloModule : NancyModule
    {
        public HelloModule()
        {
            After.AddItemToEndOfPipeline((ctx) => ctx.Response
                .WithHeader("Access-Control-Allow-Origin", "*")
                .WithHeader("Access-Control-Allow-Methods", "POST,GET")
                .WithHeader("Access-Control-Allow-Headers", "Accept, Origin, Content-type"));

            Get["/hello"] = parameters =>
            {
                Console.WriteLine("MESSAGE ARRIVED");
                return "Hello World";
            };
        }
    }
}
