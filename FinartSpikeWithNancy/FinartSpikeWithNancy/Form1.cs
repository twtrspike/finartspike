﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinartSpikeWithNancy
{
    public partial class Form1 : Form
    {
        private LocalServer localServer;

        public Form1()
        {
            InitializeComponent();
            SetBrowserEmulationMode();
            localServer = new LocalServer();
            localServer.StartServer();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string URL = textBoxForURL.Text;
            try
            {
                webBrowser.Navigate(URL);
            }
            catch (Exception err)
            {
                Console.WriteLine("Exception " + err.Message);
            }
        }

        private void SetBrowserFeatureControlKey(string feature, string appName, uint value)
        {
            using (var key = Registry.CurrentUser.CreateSubKey(
                String.Concat(@"Software\Microsoft\Internet Explorer\Main\FeatureControl\", feature),
                RegistryKeyPermissionCheck.ReadWriteSubTree))
            {
                key.SetValue(appName, (UInt32)value, RegistryValueKind.DWord);
            }
        }

        public void SetBrowserEmulationMode()
        {
            var fileName = System.IO.Path.GetFileName(Process.GetCurrentProcess().MainModule.FileName);

            if (String.Compare(fileName, "devenv.exe", true) == 0 || String.Compare(fileName, "XDesProc.exe", true) == 0)
                return;
            UInt32 mode = 10000;
            SetBrowserFeatureControlKey("FEATURE_BROWSER_EMULATION", fileName, mode);

        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            localServer.StopServer();
        }
    }
}
