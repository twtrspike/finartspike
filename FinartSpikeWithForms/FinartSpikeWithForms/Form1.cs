﻿using Microsoft.Win32;
using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace FinartSpikeWithForms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            // WPF browser actiginda compatibility mode ile birlikte aciyor (IE7) 
            // Bu durumda redux calismiyor. Bunu engellemek icin asagidaki method cagriliyor
            // Registry de bu uygulama icin compatibilty mode ayarlaniyor.
            // @alperk
            SetBrowserEmulationMode();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            string URL = textBoxForURL.Text;
            try
            {
                webBrowser.Navigate(URL);
            }
            catch (Exception err)
            {
                Console.WriteLine( "Exception " + err.Message );
            }
        }

        private void webBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {

            HtmlDocument doc = webBrowser.Document;

            HtmlElement bridgeElement = doc.GetElementById("bridgeElement");
            if (bridgeElement != null)
            {
                bridgeElement.AttachEventHandler("onclick", new EventHandler(ClickEventHandler));
                
            }
        }

        private void ClickEventHandler(object sender, EventArgs args)
        {
            
            Console.WriteLine("Event detected");
            
            
        }

        private void SetBrowserFeatureControlKey(string feature, string appName, uint value)
        {
            using (var key = Registry.CurrentUser.CreateSubKey(
                String.Concat(@"Software\Microsoft\Internet Explorer\Main\FeatureControl\", feature),
                RegistryKeyPermissionCheck.ReadWriteSubTree))
            {
                key.SetValue(appName, (UInt32)value, RegistryValueKind.DWord);
            }
        }

        public void SetBrowserEmulationMode()
        {
            var fileName = System.IO.Path.GetFileName(Process.GetCurrentProcess().MainModule.FileName);

            if (String.Compare(fileName, "devenv.exe", true) == 0 || String.Compare(fileName, "XDesProc.exe", true) == 0)
                return;
            UInt32 mode = 10000;
            SetBrowserFeatureControlKey("FEATURE_BROWSER_EMULATION", fileName, mode);

        }

    }
}
