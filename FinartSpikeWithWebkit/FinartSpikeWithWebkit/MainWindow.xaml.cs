﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Awesomium.Core;

namespace FinartSpikeWithWebkit
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            webControl.Source = urlTextBox.Text.ToUri();

        }

        private void webControl_DocumentReady(object sender, DocumentReadyEventArgs e)
        {
            JSValue jsExecutionResult = webControl.ExecuteJavascriptWithResult("document.getElementById('bridgeElement')");
            Console.WriteLine(jsExecutionResult.ToString());
            if (jsExecutionResult.IsObject)
            {
                Console.WriteLine("Element found ");
                JSObject jsObject = jsExecutionResult;
                jsObject.Bind("onclick", new JSFunctionHandler(elementOnclick));
            }
        }

        private JSValue elementOnclick(JSValue[] arguments)
        {
            Console.WriteLine("Web event caught");
            return JSValue.Undefined;
        }
    }
}
