# README #

.NET related spikes for opening a browser in .NET Forms and accessing event information

## Notes ##

WPF Event Listener :
http://stackoverflow.com/questions/9110388/web-browser-control-how-to-capture-document-events
https://support.microsoft.com/en-us/kb/312777

Browser emulation mode 
https://blog.tallan.com/2014/04/15/setting-compatibility-mode-of-a-wpf-web-browser/


Nancy self hosting 
https://github.com/NancyFx/Nancy/wiki/Self-Hosting-Nancy

Nancy CORS issue
http://stackoverflow.com/questions/26171749/getting-cors-to-work-with-nancy

Alternative Browsers
http://www.codeproject.com/Tips/825526/Csharp-WebBrowser-vs-Gecko-vs-Awesomium-vs-OpenWeb

http://thingsimplied.com/javascript-events-in-net-wpf-awesomium/